use nix::mount::{mount, MsFlags};
use nix::unistd::execv;
use std::env;
use std::ffi::CString;
use std::fs::write;
use std::os::unix::ffi::OsStrExt;
use std::process::{id, Command};

fn real_main() -> anyhow::Result<()> {
    env::set_var("RUST_BACKTRACE", "full");

    let args: Result<Vec<_>, _> = env::args_os().map(|x| CString::new(x.as_bytes())).collect();

    mount(
        Some("/nix/store"),
        "/nix/store",
        None::<&str>,
        MsFlags::MS_RDONLY | MsFlags::MS_BIND,
        None::<&str>,
    )?;

    Command::new("/nix/var/nix/profiles/system/activate")
        .env("LANG", "C.UTF-8")
        .spawn()?;

    execv(
        CString::new("/nix/var/nix/profiles/system/systemd/lib/systemd/systemd")?.as_c_str(),
        &args?,
    )?;

    Ok(())
}

fn main() {
    let result = real_main();
    // we will only get here if something fails
    write(format!("/yoink.{}.err", id()), format!("{:?}", result)).unwrap();
}
