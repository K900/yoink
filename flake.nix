{
  description = "0upti.me API";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable-small";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = {
    self,
    nixpkgs,
    flake-utils,
    ...
  }:
    flake-utils.lib.eachDefaultSystem
    (
      system: let
        pkgs = nixpkgs.legacyPackages.${system};
      in {
        packages.yoink = pkgs.rustPlatform.buildRustPackage rec {
          name = "yoink";
          version = "1.0.0";

          src = ./.;
          cargoLock = {
            lockFile = ./Cargo.lock;
          };
        };

        devShell = pkgs.mkShell {
          RUST_SRC_PATH = "${pkgs.rust.packages.stable.rustPlatform.rustLibSrc}";

          inputsFrom = builtins.attrValues self.packages.${system};
          buildInputs = [pkgs.cargo-outdated pkgs.rustfmt pkgs.clippy];
        };
      }
    );
}
